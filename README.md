# 1819 | RecycLivre x Latitudes

_Projet inno S8_

## Contexte
* RecycLivre est une entreprise qui vend des livres d’occasion sur Internet (Amazon, PriceMinister, etc).
* La mission sociale de RecycLivre s'appuie sur deux piliers :
 - l'écologie. RecycLivre cherchent à réduire au maximum leurs émissions de CO2 et financent des projets de réduction de CO2.
 - la  solidarité. RecycLivre s'engage à reverser 10% de leur revenus à des associations qui favorisent la culture (lutte contre l'illetrisme). Egalement, RecycLivre est en partenariat avec Log'ins, entreprise qui emploie exclusivement des personnes en réinsertion professionnelle. Ces dernières travaillent dans l'entrepôt de RecycLivre.
* 3 000 livres vendus par jours. Plus de 1 M€ de bénéfice est reversé à des associations.

* Vous pouvez en savoir plus sur leur site https://www.recyclivre.com/

## À propos du projet
* Enjeu pour la structure : RecycLivre a un fort impact social, et subventionne de nombreuses associations partenaires. Aussi, plus l'entreprise génère du bénéfice sur les livres revendus, plus elle pourra reverser de l'argent à ces associations. L'enjeu est donc, à partir des données marketing récoltées par Recyclivre, d'optimiser les prix d'entrée sur le marché afin de vendre au meilleur prix leurs produits. RecycLivre aimerait moins dépendre de la concurrence pour le pricing de ses produit, et l’entreprise a le potentiel de faire ça, vu les volumes de livres qui leur sont données chaque jour (environ 7000 livres/jour).
* Pour un livre (référencé par son ISBN) à un prix p donné, déterminer en combien de temps le livre sera vendu. Cet objectif revient à répondre à la question suivante : à quel prix p vendre un livre pour qu’il soit vendu rapidement ? lentement ?

- La première étape sera de prédire le flux entrant pour un livre donné (offre que peut proposer RecycLivre sur Amazon).
- La deuxième étape sera d’évaluer la demande des particuliers (achetant sur Amazon) pour un livre donné (les données ne nous indiquent pas le volume de ventes pour chaque livre).
- La troisième étape sera d’utiliser les résultats des deux premières étapes pour construire le modèle qui va nous donner l’influence du prix sur le temps de vente d’un livre.
Ainsi, on aura identifié les livres dont il faut baisser le prix au maximum pour en vendre beaucoup (plus que la concurrence) et ceux pour lesquels le prix peut être plus haut car l'offre et faible et la demande forte.



* Le cadre dans lequel cela s'inscrit : Tech for Good Explorers.
Un programme pédagogique innovant au cœur des écoles et universités
Nous intégrons le programme Tech for Good Explorers au sein de nos établissements partenaires, afin d'apporter une dimension Tech for Good à leur cursus. En participant au programme, les étudiant.e.s mobilisent leurs compétences sur des projets qui mêlent technologie et intérêt général. Chaque équipe d'étudiant.e.s est accompagnée par des mentors bénévoles Latitudes qui leur permettent de monter en compétences et de découvrir comment ils peuvent contribuer à l'intérêt général.

## Perspectives d'évolution
* Notez ici les pistes d'évolutions auquels vous avez pensé, que vous suggéreriez à des personnes qui souhaiteraient reprendre le projet.

## Sommaire
* Le sommaire du repo complet, des textes de descriptions vous sont fournis, copiez-collez-les.

## Contact

* Lisa, Camille, Vanessa et Charline : élèves en 2A à CentraleSupélec qui vont réaliser le projet.

- lisa.schillinger@student.ecp.fr
- camille.daveau@student.ecp.fr
- vanessa.long@student.ecp.fr
- charline.jean@student.ecp.fr

* Joris : le porteur de projet, développeur chez Recyclivre.

- joris.bouabdelli@recyclivre.com 

* Vincent : mentor bénévole de la communauté _Tech for Good Enthusiasts_
- liagre.vincent@gmail.com


