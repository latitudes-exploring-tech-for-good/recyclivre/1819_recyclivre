from isbnlib import meta, info, editions, desc, cover
from isbnlib.registry import bibformatters
import pandas as pd




def convert_isbn_to_infos(isbn_number, service='openl'):
    """
    Input: ISBN number (String) and service (String)
    Choose your service: 'openl' (OpenLibrary.org) or 'goob' (Google Books)

    Output: Information about the corresponding book (dictionary)
    """
    book = meta(isbn_number, service)
    book["language"] = info(isbn_number)
    book["related_editions"] = editions(isbn_number)
    book["description"] = desc(isbn_number)
    book["cover_info"] = cover(isbn_number)

    return book

print(convert_isbn_to_infos('9780446310789'))

