# -*- coding: utf-8 -*-
import pandas as pd
import os


csv_path = os.path.join("data")
df = pd.read_csv(csv_path + "/export_commande_nuitducode_etat.csv")


df.head()

df = df.loc[df["date_insertion"] != "0000-00-00"]
df = df.loc[df["date_vendu"] != "0000-00-00 00:00:00"]
df = df.dropna(how='any')

df.date_insertion = pd.to_datetime(df.date_insertion)
df.date_vendu = pd.to_datetime(df.date_vendu)

df["temps_vente"] = (df.date_vendu - df.date_insertion).apply(lambda x: int(x.days))

#Supression des éléments avec une occurence inférieure à 100

df["occurence"] = df.asin.count()
df = df.loc[df["occurence"] > 15]
#occurence = df.rename_axis('asin').reset_index('occ')

#print(occurence[occurence['occ']<5].count())
#occurence= occurence[occurence.occ > 100]

#list_occurence = occurence['asin'].tolist()

df.head()

from lifelines import KaplanMeierFitter

kmf = KaplanMeierFitter()
kmf.fit(df.temps_vente)
#kmf.plot()

kmf = KaplanMeierFitter()
kmf.fit(df[df.etat==1].temps_vente)
p1 = kmf.survival_function_

kmf = KaplanMeierFitter()
kmf.fit(df[df.etat==2].temps_vente)
p2 = kmf.survival_function_

kmf = KaplanMeierFitter()
kmf.fit(df[df.etat==3].temps_vente)
p3 = kmf.survival_function_

kmf = KaplanMeierFitter()
kmf.fit(df[df.etat==4].temps_vente)
p4 = kmf.survival_function_

import matplotlib.pyplot as plt

ax = plt.subplot(111)
plt.plot(p1.index, p1.KM_estimate, label="1")
plt.plot(p2.index, p2.KM_estimate, label="2")
plt.plot(p3.index, p3.KM_estimate, label="3")
plt.plot(p4.index, p4.KM_estimate, label="4")
plt.xlim(0, 150)
plt.ylim(0.3, 1)
plt.legend()
plt.show()