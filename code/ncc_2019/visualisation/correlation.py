#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sat Mar 16 11:22:41 2019

@author: camilledaveau
"""

import pandas as pd
import seaborn as sns
import matplotlib.pyplot as plt
import numpy as np
#from utils.convert_isbn_infos import convert_isbn_to_infos
#from utils.convert_book_vector import convert_book_to_vector
from sklearn.tree import DecisionTreeRegressor
from sklearn.metrics import mean_absolute_error
from sklearn.model_selection import train_test_split

### Traitement des donnees###

raw_sql = pd.read_csv('/Users/camilledaveau/Desktop/inno.nosync/1819_recyclivre/code/ncc_2019/data/export_commande_nuitducode_etat.csv')
columns = ["asin", "date_vendu", "date_insertion", "etat", "prix_ttc", "fdp_ttc"]
treated_sql = raw_sql[columns].copy(deep=True)

print(treated_sql.describe())

treated_sql = treated_sql.loc[treated_sql["date_insertion"] != "0000-00-00"]
treated_sql = treated_sql.loc[treated_sql["date_vendu"] != "0000-00-00 00:00:00"]

treated_sql["date_insertion"] = pd.to_datetime(treated_sql.date_insertion)
treated_sql["date_vendu"] = pd.to_datetime(treated_sql.date_vendu)

days_stocked = (treated_sql["date_vendu"] - treated_sql["date_insertion"])
days_stocked = pd.to_numeric(days_stocked)
days_stocked /= (864 * 10 ** 11)
treated_sql["jours_stock"] = days_stocked

treated_sql["month"] = pd.DatetimeIndex(treated_sql['date_vendu']).month
#dataframe.apply(convert_book_to_vector, axis=1)
treated_sql["prix_total"] = treated_sql["prix_ttc"] + treated_sql["fdp_ttc"]


#Supression des éléments avec une occurence inférieure à 100


occurence = treated_sql['asin'].value_counts()


occurence = occurence.rename_axis('asin').reset_index(name='occ')
print(occurence)
print(occurence[occurence['occ']<5].count())
#occurence= occurence[occurence.occ > 100]
#
#list_occurence = occurence['asin'].tolist()
#
#def process (asin) : 
#    if asin in list_occurence : 
#        return True
#    else : 
#        return False
#    
#treated_sql['take']=treated_sql['asin'].apply(process) 
#treated_sql.loc(treated_sql['take']== True)
#
#print(treated_sql.describe())


#### Modele
#
#book_features= ['etat', 'month', 'prix_total']
#X = treated_sql[book_features]
#print (X.head())
#print(X.describe())
#y= treated_sql['jours_stock']
#
#
# split data into training and validation data, for both features and target
# The split is based on a random number generator. Supplying a numeric value to
# the random_state argument guarantees we get the same split every time we
#
#train_X, val_X, train_y, val_y = train_test_split(X, y, random_state = 0)
## Define model
#book_model = DecisionTreeRegressor(random_state = 0)
## Fit model
#book_model.fit(train_X, train_y)
#
## get predicted prices on validation data
#predictions = book_model.predict(val_X)
#
#print(mean_absolute_error(val_y, val_predictions))
#


#columns4 = ["etat", "statut","prix_ttc", "fdp_ttc", "date_mel","date_vendu" ]
#sql4 = sql2[columns4]
#
#sql4 = sql4.loc[sql4['date_mel'] != '0000-00-00'] #on prend que les données ou la date n'est pas 000
#sql4 = sql4.loc[sql4['date_vendu'] != '0000-00-00']
#sql4['date_mel'] = pd.to_datetime(sql4.date_mel) #conversion en date
#sql4['date_vendu'] = pd.to_datetime(sql4.date_vendu)
#sql4["days"] = (sql4["date_vendu"] - sql4["date_mel"]) #Création d'une nouvelle colonne
#sql4['days'] = pd.to_numeric(sql4.days)
#missing_val_count_by_column = (sql4.isnull().sum())
#print(missing_val_count_by_column[missing_val_count_by_column > 0])
#sql4['days'].value_counts()
#
### plot avec scatter prix en fonction du délai de vente et états
#sql4['days'] = pd.to_numeric(sql4.days) # je convertis days en float
#
#sql4["prix_tot"] = (sql4["prix_ttc"] + sql4["fdp_ttc"])
#
#del sql4['fdp_ttc']
#del sql4['prix_ttc']
#del sql4['date_mel']
#del sql4['date_vendu']
##sql4['statut']= sql4['statut'].map({'999':1,'5':0, '2':0})
#
#corr = sql4.corr()
#
#sns.heatmap(sql4.corr(), annot=True, fmt=".2f")
#plt.show()
