# 1819 | RecycLivre x Latitudes

Cette partie contient le produit développé dans le cadre du projet Recyclivre en partenariat avec Latitudes.
L'objet de notre travail a été de prédire les flux de livres entrants. Plus précisément nous allons tenter de fournir une prédiction du nombre de livre reçu dans le mois courant pour chaque référence unique (asin).

## Architecture globale
### Data : 
* Dans ce dossier on trouve tous les fichier csv dont on se sert pour réaliser le modèle prédictif. En particulier, les fichiers **export_commande_nuitducode_etat.csv** et **log_refus_livre.csv** sont les données initiales sur lequelles on va travailler. export_commande_nuitducode_etat.csv est l'historique des livre reçus et mis en ligne, il contient des informations sur l'isbn du livre, l'etat du livre, les dates d'insertion et de vente, les prix et les frais de port. Le fichier log_refus_livre.csv quant à lu iest un historique des livres reçus et qui ont été refusé (donc jamais mis en étagère et en ligne), il contient les références isbn et les dates de reception.  
* Ensuite, on va trouver dans data des fichiers intermédiaires, **treated_data.csv** et **treated_data_refus.csv**. Ce sont les fichiers créés a partir des 2 fichiers initiaux et qui contiennent les volumes entrants de données.
* Les fichier **amazon_rank.csv** et **bddisbn.csv** sont des fichiers annexes dans lesquels on a stocké des informations concernant un certain nombre de références uniques (mais pas toutes). En particulier, amazon_rank.csv contient le ranking amazon de certains livre pour des dates donées, bddsibn.csv quant à lui contient les titres, année de parution et langue d'un certain nombre de références uniques.
* Enfin le fichier **books_entrant_info.csv** croise toutes les données précédentes, c'est le fichier à partir duquel on va entrainer le modèle prédictif. On sera amener à mettre à jours ce fichier avec de nouvelles données pour pouvoir ré-entrainer le modèle et s'assurer qu'il n'y ai pas de divergances.

### Utils : 
Dans le dossier Utils on trouve tous les fichier python qui permettent de mettre en forme les données d'entrainement.
* Le fichier **get_isbn_info.py** contient des fonctions qui vont aller récupérer Titre, AnneeParution, Langue pour une liste de références isbn (clé asin dans nos donées). L'implémentation est très longue.
* Le fichier **amazon_rank.py** contient des fonctions qui permettent d'obtenir le ranking amazon pour un asin et un mois donné. Ici aussi cette opération est très longue.
* Le fichier **treat_data_livres.py** effectue les traitement des données initiales (historique des livre sacceptés et refusés) enfin d'obtenir les volumes entrats et d'associer à chaque référence les informations stockées dans bddisbn.csv et le ranking si on le souhaite. Ce code fait donc appel au 2 fichiers pythons précédents. Une fois les données traitées on pourras les utiliser pour entrainer le modèle prédictif.

### Model :
Dans ce dossier on trouve le code pour entrainer le modèle et obtenir des prédiction sur les volumes entrants et en particulier on va prédire le nombre d'exemplaire que l'on va recevoir dans le mois pour chaque référence asin précise.
Une fois entrainé le modèle est sauvegardé sous forme de fichier .pkl que l'on peut alors utilisé seul et pédire en temps réel.

### Visualisation :
Ici il s'agit d'observer les données sous forme de graphiques.
* Dans le dossier **Algo** on trouve des fichier pythons, ces fichier contiennent les fonctions qui permettent de tracer les différents graphes.
* Dans le dossir **Visuels** on trouve des exemples de graohe qui ont été tracés grâce aux fonctions précédentes.

### Ncc_2019 : 
Ici on trouve le travail qui avait été effectué lors de la Nuit du Code Citoyen le 16 mars. Certains points on été réutilisés par la suite mais ce travail reste dissocié de notre travail en temps que projet inno.

## Implémentation

On considère que l'on dispose de fichier csv contenant les données à traiter sous la même forme que les 2 fichier **export_commande_nuitducode_etat.csv** et **log_refus_livre.csv**. On considère aussi que l'on a déjà traité et mis en forme une première les données fournies en début de projet.

### Point sur les variables de volumes
 * **occur** : il s'agit du nombre de livre reçus et accepté durant un mois donné d'une année donnée (qui correspondent à la date insertion) pour une référence asin donnée
 * **occur_refus** : même chose mais pour les livres refusés
 * **nb_tot_accept** : nb total d'apparition d'un référence unique asin dans l'emsemble des données disponibles pour les livres acceptés.
 * **nb_tot_refus** : même chose pour les livres refusés.
 * **occur_tot** :  occur + occur_refus.
 * **nb_tot_books** : nb_tot_accept + nb_tot_refus.
 * **occur_tot_normalise** : occur_tot / nb_tot_books.

### Prédiction à partir d'un modèle déjà entrainé
Le modèle déjà entrainé et stocké dans le fichier **model.pkl**. Pour s'en servir et obtenir des prédictions sur un tableau de données de quelques lignes (asin, mois, annee, volume total, temps depuis la parution du livre, etc...), il suffit de faire tourner la cellule 'Travail sur pkl' du fichier CodeCamille21mai_et_pkl.py, un fichier avec les prédiction sera alors créé et enregistré dans le répertoire courant.

### Mise à jours des données d'entrainement
Considérons maintenant que quelque temps s'est écoulé depuis la mise en place du modèle prédictif et on se rend compte que les prédictions ne collent plus assez bien à la réalité. Il faut donc entrainer à nouveau le modèle avec de nuvelles données plus récentes. On va devoir mettre en forme à nouveau ces données
* Pour cela la **première solution envisageable** est de garder les données qu'on avait déjà traitées précedement, puis de faire les même traitements sur les nouvelles données et de concaténer les 2 tableaux de données. A ce moment la on fait face à un porblème, il faut harmoniser les variables de volumes. Cette opération prend beaucoup de temps, ce n'est pas necessairement la meilleures solution.
* La **deuxième solution** est de complètement traiter à nouveau les données qu'on veut utiliser pour entrainer le modèle, ce qui revient à ré-executer le code treat_data_livres.py en totalité. Si cela fait trop de données il est possible d'exclure les données qui datent de plusieurs années.

Note: pour gagner du temps, ne pas aller chercher le ranking amazon.

## Suite du projet
Nous nous sommes arrêtés à la première étapes qui consiste à prédire le flux entrant de livre chez Recyclivre. La prochaine étape sera d'évaluer et de prédire la demande afin de fournir une meilleure évaluation des prix à fixer du côté de Recyclivre.

Vous trouverez plus de détails sur le contexte et les perspectives d'évolutions du projet dans le dossier parent.
L'accès à la documentation détaillée se fait dans le dossier "[documentation](https://gitlab.com/latitudes-exploring-tech-for-good/recyclivre/1819_recyclivre/tree/master/documentation)".