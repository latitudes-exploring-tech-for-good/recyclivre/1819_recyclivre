#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sun May 19 20:52:28 2019

@author: camilledaveau
"""
# Import the necessary modules and libraries
import pandas as pd
from sklearn.tree import DecisionTreeRegressor
import os
from sklearn.model_selection import train_test_split
import sklearn.metrics
from sklearn.metrics import mean_absolute_error
import joblib


# Traitement des données


csv_path = os.path.join("..", "data")
df0 = pd.read_csv(csv_path + "/amazon_rank.csv")


df0['gap'] = df0['annee'] - df0['AnneeParution']

columns = ['asin', 'date_vendu', 'date_insertion', 'etat', 'mois',
       'annee', 'occur', 'occur_refus', 'nb_tot_accept', 'nb_tot_refus',
       'occur_tot', 'nb_tot_books', 'occur_tot_normalise', 'Titre',
       'AnneeParution', 'rank', 'gap']


df1=df0[columns]
df1=df1.dropna(axis=0)




df = df1[df1.annee > 2015]

# occur : nombre de livre recu et accepté sur le mois et l'année, peu importe l'etat
# occur_refus : nombre de livre refusé sur le mois
# occur tot : occurences dans le mois
# nb_tot_accepte : nombre total de livre accepté depuis toujours
#nb tot refus : nombre total de livre refusé depuis toujours
# nb tot book : nombre total de libre depuis tjrs
#occur_tot_normalisé= occur tot / nbr total depuis toujours

book_features = ['etat','mois','annee','nb_tot_books','gap', 'rank']
X = df1[book_features]
y=df1.occur_tot

print(df.describe())


# Définition du modèle


train_X, val_X, train_y, val_y = train_test_split(X, y,test_size=0.33, random_state = 1) 
incoming_model = DecisionTreeRegressor()

incoming_model.fit(train_X, train_y)

val_y_predictions=incoming_model.predict(val_X)
val_y_test = incoming_model.predict(train_X)


# Analyse des résultats

#print(incoming_model.score(X,y))

score_train = sklearn.metrics.r2_score(train_y, val_y_test)
score_test = sklearn.metrics.r2_score(val_y, val_y_predictions)

print (score_train, score_test)

#Pour les donnees > 2016 : 1.0 0.9962820227590086
# pour toutes les données : 1.0 0.9962297220697468

#Mean absolute error

tr_val_mae = mean_absolute_error(val_y,val_y_predictions)
print ("Validation MAE for Tree Regressor Model: {}".format(tr_val_mae))

#Validation MAE for Tree Regressor Model: 0.2863993450164721

#Performance du modèle sur le set d'entraînement train_X
trainX_mae = mean_absolute_error(train_y,val_y_test)
print ("Validation MAE for Tree Regressor Model (on trainX): {}".format(trainX_mae))
#Validation MAE for Tree Regressor Model: 0.0

"""
#%% On enregistre le modèle sous format pkl

joblib.dump(incoming_model, "TreeRegressorModel.pkl")

#
# Load model from file
TreeRegressorClassifier = joblib.load("TreeRegressorModel.pkl")



#Prenons les 10 dernières lignes de amazon_rank.csv
#Ces lignes permettent de comprendre comment utiliser le modèle qu'on a enregistré
DfDemo= df1.tail(10)
# il faut donner au modèle des données au même format que sur lesquelles il s'est entraîné
XDemo = DfDemo[book_features]
predictions = TreeRegressorClassifier.predict(XDemo)


#On exporte les prédictions dans un fichier csv
submission = pd.DataFrame({'asin':DfDemo['asin'],'occurMMAAAA':predictions})
filename = 'TreeRegressorPredictions.csv'
submission.to_csv(filename,index=False)

print('Saved file: ' + filename)
"""