# -*- coding: utf-8 -*-

import pandas as pd

from sklearn import ensemble
from sklearn.model_selection import GridSearchCV
import os
from sklearn.model_selection import train_test_split
import sklearn.metrics
from sklearn.metrics import mean_absolute_error
from sklearn.metrics import mean_squared_error


# Traitement des données


csv_path_1 = os.path.join("data")
df0 = pd.read_csv("/Users/charl/Documents/Centrale/Latitudes/1819_recyclivre/code/data/amazon_rank.csv")

df0['gap'] = df0['DeltaParution']-2019+ df0['annee']

columns = ['asin', 'date_vendu', 'date_insertion', 'etat', 'mois',
       'annee', 'occur', 'occur_refus', 'nb_tot_accept', 'nb_tot_refus',
       'occur_tot', 'nb_tot_books', 'occur_tot_normalise', 'Titre',
       'AnneeParution', 'rank', 'gap']


df1=df0[columns]
df1=df1.dropna(axis=0)




df = df1[df1.annee > 2015]

# occur : nombre de livre recu et accepté sur le mois et l'année, peu importe l'etat
# occur_refus : nombre de livre refusé sur le mois
# occur tot : occurences dans le mois
# nb_tot_accepte : nombre total de livre accepté depuis toujours
#nb tot refus : nombre total de livre refusé depuis toujours
# nb tot book : nombre total de libre depuis tjrs
#occur_tot_normalisé= occur tot / nbr total depuis toujours

book_features = ['etat','mois','annee','nb_tot_books','gap', 'rank']
X = df[book_features]
y=df.occur_tot

print(df.describe())


# Définition du modèle


train_X, val_X, train_y, val_y = train_test_split(X, y,test_size=0.33, random_state = 1) 




params = {'n_estimators': 500, 'max_depth': 5, 'min_samples_split': 4, 'learning_rate': 0.5, 'loss': 'ls'}
incoming_model = ensemble.GradientBoostingRegressor()


incoming_model.fit(train_X, train_y)

val_y_predictions=incoming_model.predict(val_X)
val_y_test = incoming_model.predict(train_X)


# Analyse des résultats

#print(incoming_model.score(X,y))

score_train = sklearn.metrics.r2_score(train_y, val_y_test)
score_test = sklearn.metrics.r2_score(val_y, val_y_predictions)

print ("Score train : ",score_train, " Score test : ",score_test)

#Pour les donnees > 2016 : 1.0 0.9962820227590086
# pour toutes les données : 1.0 0.9962297220697468

#Mean absolute error

tr_val_mae = mean_absolute_error(train_y,val_y_test)
print ("Validation MAE for Gradient Boosting Model: {}".format(tr_val_mae))

#Validation MAE for Tree Regressor Model: 0.2863993450164721

#Mean squared error

tr_val_mse = mean_squared_error(val_y,val_y_predictions)
print ("Validation MSE for Gradient Boosting Model: {}".format(tr_val_mse))