import pandas as pd
from sklearn.metrics import mean_absolute_error
import os
from sklearn.model_selection import train_test_split
from sklearn.tree import DecisionTreeRegressor
from sklearn.ensemble import RandomForestRegressor
from sklearn.metrics import mean_absolute_error
import joblib


# Traitement des données



csv_path = os.path.join("..", "data")
df0 = pd.read_csv(csv_path + "/amazon_rank.csv")


df0['gap'] = df0['annee'] - df0['AnneeParution']

columns = ['asin', 'date_vendu', 'date_insertion', 'etat', 'mois',
       'annee', 'occur', 'occur_refus', 'nb_tot_accept', 'nb_tot_refus',
       'occur_tot', 'nb_tot_books', 'occur_tot_normalise', 'Titre',
       'AnneeParution', 'rank', 'gap']


df1=df0[columns]
df1=df1.dropna(axis=0)

# Définition du modèle

book_features = ['etat','mois','annee','nb_tot_books','gap', 'rank']
X = df1[book_features]
y=df1.occur_tot

train_X, val_X, train_y, val_y = train_test_split(X, y,test_size=0.33, random_state = 1) 

def findBestParametersRF():
    data_train, data_test, labels_train, labels_test = train_test_split(\
    X, y, test_size=0.33, random_state=1)
    minimum_mae = 10000
    def train_rf(n_est, depth):
        rf = RandomForestRegressor(n_estimators= n_est, max_depth= depth, n_jobs = -1)
        rf_model = rf.fit(data_train,labels_train)
        labels_pred = rf_model.predict(data_test)
        rf_val_mae = mean_absolute_error(labels_test,labels_pred)
        return rf_val_mae

    for n_est in [10, 100, 400,500,600,700]:
        for depth in [80, 100, 300, 500, None]:
            current_mae = train_rf(n_est,depth)
            
        
            if current_mae < minimum_mae:
                minimum_mae = current_mae
                print("The best number of estimators is", n_est)
                print("The best depth is", depth)
                print("For these parameters, the MAE is", current_mae)

# Define the model. Set random_state to 1
rf_model = RandomForestRegressor(n_estimators = 600, max_depth = 100, random_state=1)

# fit your model
rf_model.fit(train_X, train_y)


# Calculate the mean absolute error of your Random Forest model on the validation data

rf_val_predictions = rf_model.predict(val_X)
rf_val_mae = mean_absolute_error(val_y,rf_val_predictions)

print("Validation MAE for Random Forest Model: {}".format(rf_val_mae))
#Validation MAE for Random Forest Model: 0.3400183239439365

#Performance du modèle sur le set d'entraînement trainX
rf_trainX_predictions = rf_model.predict(train_X)
rf_trainX_mae = mean_absolute_error(train_y, rf_trainX_predictions)
print("Validation MAE for Random Forest Model (trainX): {}".format(rf_trainX_mae))


"""
#%% On enregistre le modèle sous format pkl

joblib.dump(rf_model, "RandomForestModel.pkl")

#
# Load model from file
RandomForestClassifier = joblib.load("RandomForestModel.pkl")



#Prenons les 10 dernières lignes de amazon_rank.csv
#Ces lignes permettent de comprendre comment utiliser le modèle qu'on a enregistré
DfDemo= df1.tail(10)
# il faut donner au modèle des données au même format que sur lesquelles il s'est entraîné
XDemo = DfDemo[book_features]
predictions = RandomForestClassifier.predict(XDemo)

#On exporte les prédictions dans un fichier csv
submission = pd.DataFrame({'asin':DfDemo['asin'],'occurMMAAAA':predictions})
filename = 'RandomForestClassifierPredictions.csv'
submission.to_csv(filename,index=False)

print('Saved file: ' + filename)

"""