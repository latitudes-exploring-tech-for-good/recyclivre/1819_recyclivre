# -*- coding: utf-8 -*-
"""
Created on Tue May 21 09:45:54 2019

@author: Lenovo

Exploration du fichier csv fourni par RecycLivre pour la Nuit du Code Citoyen
Il s'agit du fichier export_commande_nuitducode_etat.csv

Se placer dans le répertoire '1819_recyclivre/code'
"""

#%%import 
import pandas as pd
import os
import matplotlib.pyplot as plt
import seaborn as sns
from Utils.get_isbn_info import convert_isbn_to_infos

#%% Visualisation des données

#Commentaire sur price_wrt_time : 
#Motivation : est-ce que plus un livre est gardé longtemps, plus son prix risque d’être bas ?
#En réalité : pas vraiment de lien entre le prix et la durée de stockage du livre dans l'entrepôt
def price_wrt_time(isbn):
    """
    Input: ISBN-13 (String)
    Output: Graph showing price with respect to time for
            that ISBN
    """

    #Use the line below if you want to run the code on the server (Webmin)
    #raw_sql = pd.read_csv("/home/techfg/1819_recyclivre/code/data/export_commande_nuitducode_etat.csv")
    
    #Use the lines below if you are running the code from your computer
    csv_path = os.path.join("../..", "data")
    raw_sql = pd.read_csv(csv_path + "/export_commande_nuitducode_etat.csv")


    # Choose columns that are relevant to the problem
    columns = ["asin", "date_vendu", "date_insertion", "etat", "prix_ttc", "fdp_ttc"]
    treated_sql = raw_sql[columns].copy(deep=True)

    # Filter out with isbn
    treated_sql = treated_sql.loc[treated_sql["asin"] == isbn]

    # Filter out 'year 0'
    treated_sql = treated_sql.loc[treated_sql["date_insertion"] != "0000-00-00"]
    treated_sql = treated_sql.loc[treated_sql["date_vendu"] != "0000-00-00 00:00:00"]

    treated_sql["date_insertion"] = pd.to_datetime(treated_sql.date_insertion)
    treated_sql["date_vendu"] = pd.to_datetime(treated_sql.date_vendu)
    
    days_stocked = (treated_sql["date_vendu"] - treated_sql["date_insertion"])
    days_stocked = pd.to_numeric(days_stocked)
    days_stocked /= (864 * 10 ** 11)
    treated_sql["jours_stock"] = days_stocked

    treated_sql["prix_ttc"] = raw_sql.prix_ttc
    treated_sql["fdp_ttc"] = raw_sql.fdp_ttc
    treated_sql["prix_total"] = treated_sql["prix_ttc"] + treated_sql["fdp_ttc"]


    plt.figure()
    plt.scatter(treated_sql["prix_total"], treated_sql["jours_stock"], c=treated_sql.etat)
    plt.xlabel('prix_ttc')
    plt.ylabel('jours_stock')
    #fonctionne mais je n'arrive pas à mettre les labels indiquant l'état du livre

    try:
        book_info = convert_isbn_to_infos(str(isbn))
        #Displays title of the book and its "main" author (first one)
        plt.title("\"" + book_info["Title"] + "\"" + ", par " + book_info["Authors"][0])
    except:
        plt.title("ISBN: " + isbn)

    sns.lmplot('prix_ttc', 'jours_stock', hue='etat',data=treated_sql, fit_reg=False)
    #ici on voit les labels des points (état du livre) c'est good

    sns.lmplot('prix_total','jours_stock', hue='etat',data=treated_sql, fit_reg=False)
    #même chose pour le prix total

    plt.show()

price_wrt_time('2213631301')
#%%
#Commentaire sur price_wrt_period et sur prix_vs_mois : 
# Motivation : y a-t-il des périodes (date de vente, ou mois de vente)où les prix sont toujours plus hauts ?
# En réalité : le prix varie trop, on ne voit pas du tout les effets de saisonnalité

def price_wrt_period(isbn):
    """
    Input: ISBN-13 (String)
    Output: Graph showing price with respect to the period of 
            the year for that ISBN
    """
    
     #Use the line below if you want to run the code on the server (Webmin)
    #raw_sql = pd.read_csv("/home/techfg/1819_recyclivre/code/data/export_commande_nuitducode_etat.csv")
    
    #Use the lines below if you are running the code from your computer
    csv_path = os.path.join("../..", "data")
    raw_sql = pd.read_csv(csv_path + "/export_commande_nuitducode_etat.csv")

    # Choose columns that are relevant to the problem
    columns = ["asin", "date_vendu", "date_insertion", "etat", "prix_ttc", "fdp_ttc"]
    treated_sql = raw_sql[columns].copy(deep=True)

    # Filter out with isbn
    treated_sql = treated_sql.loc[treated_sql["asin"] == isbn]

    # Filter out 'year 0'
    treated_sql = treated_sql.loc[treated_sql["date_vendu"] != "0000-00-00 00:00:00"]

    treated_sql["date_vendu"] = pd.to_datetime(treated_sql.date_vendu)

    treated_sql["prix_ttc"] = raw_sql.prix_ttc
    treated_sql["fdp_ttc"] = raw_sql.fdp_ttc
    treated_sql["prix_total"] = treated_sql["prix_ttc"] + treated_sql["fdp_ttc"]


    plt.figure(1)
    #plt.scatter(treated_sql["date_vendu"], treated_sql["prix_total"], c=treated_sql.etat)
    plt.plot(treated_sql["date_vendu"], treated_sql["prix_total"])
    plt.xlabel('jour_annee')
    plt.ylabel('prix_total')

    try:
        book_info = convert_isbn_to_infos(str(isbn))
        # Displays title of the book and its "main" author (first one)
        plt.title("\"" + book_info["Title"] + "\"" + ", par " + book_info["Authors"][0])
    except:
        plt.title("ISBN: " + isbn)
    #fonctionne mais je n'arrive pas à mettre les labels

    #sns.lmplot('prix_ttc', 'date_vendu', hue='etat', data=treated_sql, fit_reg=False)
    #ici on voit les labels des point c'est good

    #sns.lmplot('prix_total','date_vendu', hue='etat', data=treated_sql, fit_reg=False)
    #même chose pour le prix total

    plt.show()

price_wrt_period('2213631301')

def prix_vs_mois(isbn):
    
    #Use the line below if you want to run the code on the server (Webmin)
    #raw_sql = pd.read_csv("/home/techfg/1819_recyclivre/code/data/export_commande_nuitducode_etat.csv")
    
    #Use the lines below if you are running the code from your computer
    csv_path = os.path.join("../..", "data")
    raw_sql = pd.read_csv(csv_path + "/export_commande_nuitducode_etat.csv")

    # Choose columns that are relevant to the problem
    columns = ["asin", "date_vendu", "date_insertion", "etat", "prix_ttc", "fdp_ttc"]
    treated_data = raw_sql[columns].copy(deep=True)
    treated_data['prix_total']= treated_data['prix_ttc']+treated_data['fdp_ttc']


    # Filter out with isbn
    treated_data = treated_data.loc[treated_data["asin"] == isbn]

    # Filter out 'year 0'
    treated_data = treated_data.loc[treated_data["date_insertion"] != "0000-00-00"]
    treated_data = treated_data.loc[treated_data["date_vendu"] != "0000-00-00 00:00:00"]

    treated_data["date_insertion"] = pd.to_datetime(treated_data.date_insertion)
    treated_data["date_vendu"] = pd.to_datetime(treated_data.date_vendu)
    
    treated_data['mois_vente'] = pd.DatetimeIndex(treated_data['date_vendu']).month

    plt.figure('Prix')
    plt.scatter(treated_data['mois_vente'], treated_data['prix_total'], c=treated_data.mois_vente)
    plt.xlabel('mois de vente')
    plt.ylabel('prix de vente')
    try:
        book_info = convert_isbn_to_infos(str(isbn))
        # Displays title of the book and its "main" author (first one)
        plt.title("\"" + book_info["Title"] + "\"" + ", par " + book_info["Authors"][0])
    except:
        plt.title("ISBN: " + isbn)
    #plt.savefig(isbn + ' prix_mois')

#prix_vs_mois('222105587X')
#prix_vs_mois('2226142886')
#prix_vs_mois('226402383X')
#%%
#il me semble que c'est la même chose que price_wrt_time, mais juste pour l'état 4
def days_stocked_wrt_price(isbn):
    """
    Input: ISBN-13 (String)
    Output: Graph showing number of days spent in stock with respect 
            to the price of the book
    """

    #Use the line below if you want to run the code on the server (Webmin)
    #raw_sql = pd.read_csv("/home/techfg/1819_recyclivre/code/data/export_commande_nuitducode_etat.csv")
    
     #Use the lines below if you are running the code from your computer
    csv_path = os.path.join("../..", "data")
    raw_sql = pd.read_csv(csv_path + "/export_commande_nuitducode_etat.csv")
    
    # Choose columns that are relevant to the problem
    columns = ["asin", "date_vendu", "date_insertion", "etat", "prix_ttc", "fdp_ttc"]
    treated_sql = raw_sql[columns].copy(deep=True)

    # Filter out with isbn
    treated_sql = treated_sql.loc[treated_sql["asin"] == isbn]

    # Filter out 'year 0'
    treated_sql = treated_sql.loc[treated_sql["date_insertion"] != "0000-00-00"]
    treated_sql = treated_sql.loc[treated_sql["date_vendu"] != "0000-00-00 00:00:00"]

    treated_sql["date_insertion"] = pd.to_datetime(treated_sql.date_insertion)
    treated_sql["date_vendu"] = pd.to_datetime(treated_sql.date_vendu)
    
    days_stocked = (treated_sql["date_vendu"] - treated_sql["date_insertion"])
    days_stocked = pd.to_numeric(days_stocked)
    days_stocked /= (864 * 10 ** 11)
    treated_sql["jours_stock"] = days_stocked

    treated_sql["prix_ttc"] = raw_sql.prix_ttc
    treated_sql["fdp_ttc"] = raw_sql.fdp_ttc
    treated_sql["prix_total"] = treated_sql["prix_ttc"] + treated_sql["fdp_ttc"]


    # Affiche le délai de vente en fonction du prix pour l'état 4
    plt.figure(1)
    try:
        book_info = convert_isbn_to_infos(str(isbn))
        # Displays title of the book and its "main" author (first one)
        plt.title("\"" + book_info["Title"] + "\"" + ", par " + book_info["Authors"][0])
    except:
        plt.title("ISBN: " + isbn)

    treated_sql = treated_sql.loc[treated_sql['etat'] == 4]
    sns.swarmplot(x="jours_stock", y="prix_ttc", data=treated_sql)
    sns.catplot("jours_stock", "prix_total", "etat", data=treated_sql, kind="point", palette="muted", legend=True) 
   # plt.xticks(arange(min(treated_sql.jours_stock), max(treated_sql.jours_stock) + 1, (max(treated_sql.jours_stock) - min(treated_sql.jours_stock)) / 10))
    plt.show()


days_stocked_wrt_price('2213631301')

#%% Suite de la visualisation des données
#On s'intéresse ici aux ventes en fonction du mois
#Nombre d’exemplaires vendus ou reçus = f (mois de vente ou mois de réception)

# L'idée est de voir s'il existe des saisonnalités

# Commentaire : en regardant sur 3-4 livres, on remarque que 
#mois de vente : novembre, décembre, janvier totalisent le plus de vente
#mois de réception : janvier, octobre


def ventes_vs_mois(isbn):
    
    #Use the line below if you want to run the code on the server (Webmin)
    #raw_sql = pd.read_csv("/home/techfg/1819_recyclivre/code/data/export_commande_nuitducode_etat.csv")
    
    #Use the lines below if you are running the code from your computer
    csv_path = os.path.join("../..", "data")
    raw_sql = pd.read_csv(csv_path + "/export_commande_nuitducode_etat.csv")
    
    # Choose columns that are relevant to the problem
    columns = ["asin", "date_vendu", "date_insertion", "etat", "prix_ttc", "fdp_ttc"]
    treated_data = raw_sql[columns].copy(deep=True)
    treated_data['prix_total']= treated_data['prix_ttc']+treated_data['fdp_ttc']

    # Filter out with isbn
    treated_data = treated_data.loc[treated_data["asin"] == isbn]

    # Filter out 'year 0'
    treated_data = treated_data.loc[treated_data["date_insertion"] != "0000-00-00"]
    treated_data = treated_data.loc[treated_data["date_vendu"] != "0000-00-00 00:00:00"]

    treated_data["date_insertion"] = pd.to_datetime(treated_data.date_insertion)
    treated_data["date_vendu"] = pd.to_datetime(treated_data.date_vendu)
    
    treated_data['mois_vente'] = pd.DatetimeIndex(treated_data['date_vendu']).month
    
    plt.figure()
    count = pd.value_counts(treated_data["mois_vente"]).sort_index()
    count.plot(kind ='bar')
    plt.xlabel('mois de vente')
    plt.ylabel('nb d exemplaires vendus')
    try:
        book_info = convert_isbn_to_infos(str(isbn))
        # Displays title of the book and its "main" author (first one)
        plt.title("\"" + book_info["Title"] + "\"" + ", par " + book_info["Authors"][0])
    except:
        plt.title("ISBN: " + isbn)
    #plt.savefig(isbn + ' nb_ventes_mois')
    

def reception_vs_mois(isbn):

    #Use the line below if you want to run the code on the server (Webmin)
    #raw_sql = pd.read_csv("/home/techfg/1819_recyclivre/code/data/export_commande_nuitducode_etat.csv")
    
    #Use the lines below if you are running the code from your computer
    csv_path = os.path.join("../..", "data")
    raw_sql = pd.read_csv(csv_path + "/export_commande_nuitducode_etat.csv")
    # Choose columns that are relevant to the problem
    columns = ["asin", "date_vendu", "date_insertion", "etat", "prix_ttc", "fdp_ttc"]
    treated_data = raw_sql[columns].copy(deep=True)
    treated_data['prix_total']= treated_data['prix_ttc']+treated_data['fdp_ttc']


    # Filter out with isbn
    treated_data = treated_data.loc[treated_data["asin"] == isbn]

    # Filter out 'year 0'
    treated_data = treated_data.loc[treated_data["date_insertion"] != "0000-00-00"]
    treated_data = treated_data.loc[treated_data["date_vendu"] != "0000-00-00 00:00:00"]

    treated_data["date_insertion"] = pd.to_datetime(treated_data.date_insertion)
    treated_data["date_vendu"] = pd.to_datetime(treated_data.date_vendu)
    
    treated_data['mois_reception'] = pd.DatetimeIndex(treated_data['date_insertion']).month
    
    plt.figure()
    count = pd.value_counts(treated_data["mois_reception"]).sort_index()
    count.plot(kind ='bar')
    plt.xlabel('mois de reception')
    plt.ylabel('nb d exemplaires vendus')
    try:
        book_info = convert_isbn_to_infos(str(isbn))
        # Displays title of the book and its "main" author (first one)
        plt.title("\"" + book_info["Title"] + "\"" + ", par " + book_info["Authors"][0])
    except:
        plt.title("ISBN: " + isbn)
    #plt.savefig(isbn + ' nb_reception_mois')
    

    
    
#ventes_vs_mois('222105587X')
#reception_vs_mois('222105587X')


ventes_vs_mois('2226142886')
reception_vs_mois('2226142886')



