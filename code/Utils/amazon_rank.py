# -*- coding: utf-8 -*-
"""
Created on Fri Apr 19 18:04:55 2019

@author: Lenovo
"""

import pandas as pd
import os
import requests
import time
import json
from datetime import datetime

## pour le moment je cherche à stocker les infos d'amazon dans un dataframe
'''
start_time = time.time()
#ici j'exécute une commande shell pour créer un fichier json dans le serveur
os.chdir('/home/techfg/1819_recyclivre/code/data/')
os.system('curl -i -H "Content-Type: application/json" http://37.59.15.105/Sqs/api_az.php?asin=2207304639 > test.json')
print ("Os.system took", time.time() - start_time, "to run")
'''

#%%conversion timestamp to date
def timestamp_to_date(dater):
    pos = dater.find("s':")
    timestamp= dater[pos+4: pos+14]
    date = datetime.fromtimestamp(int(timestamp))
    return (date)

#%%Recuperation du rang
# ici j'utilise une requette directe sur l'API, ça marche bien depuis le serveur.
def amazon (asin): 
    content = requests.get ('http://37.59.15.105/Sqs/api_az.php?asin='+str(asin))
    data_json=content.json()
    data_jsonstr=str(data_json)
    data_jsonstr= data_jsonstr.replace ("'", '"')
    amazon = pd.read_json(data_jsonstr)
    amazon['dater']= amazon['dater'].astype(str)
    amazon['date'] = amazon['dater'].apply(lambda x: timestamp_to_date(x))
    amazon['date'] = pd.to_datetime(amazon.date)
    #amazon.to_csv('amazon.csv')
    #print (amazon.head())
    return (amazon)



#print (amazon('2742770313'))
#print (amazon('207036822X').head())
#print(amazon('2207304639'))




def rank (date_vendu, amazon_df):
    amazon_df['mois_amaz'] = pd.DatetimeIndex(amazon_df['date']).month
    amazon_df['annee_amaz'] =  pd.DatetimeIndex(amazon_df['date']).year
    df = amazon_df.loc[(amazon_df['annee_amaz']==date_vendu.year) & (amazon_df['mois_amaz']==date_vendu.month)]
    #df = amazon_df.loc[amazon_df['annee_amaz']==date_vendu.year]
    #df = df.loc[df['mois_amaz']==date_vendu.month]
    ranks=df['rank']
    ranks = ranks.reset_index(drop= True)
    rang = ranks.loc[0]
    return (rang)


def ranking (information_line):
    date_vendu = information_line.date_vendu
    #print(date_vendu)
    asin = information_line.asin
    #print(asin)
    if date_vendu !=0:
        try:
            amazon_df=amazon(asin)
            rang = rank(date_vendu, amazon_df)
            return(rang)
        except : 
            return(None)
    else : 
        return (-1000)

def get_rank(df):
    data_books = df.loc[df['date_vendu']!= 0]
    data_books = data_books.dropna(subset=['date_vendu'])
    data_books['date_vendu']= pd.to_datetime(data_books.date_vendu)
    data_books = data_books.reset_index(drop=True)
    data_books['rank']=data_books.apply(lambda x: ranking(x), axis=1)
    return data_books


#Si on veut paralléliser le calcul :

books = pd.read_csv('......')

def get_rank(bloc):
    print (bloc)
    books_bloc=books.loc[bloc[0] : bloc[1]]
    books_bloc['rank']=books_bloc.apply(lambda x: ranking(x), axis=1)
    os.chdir('/home/techfg/1819_recyclivre/code/data/')
    books_bloc.to_csv('rank_'+str(bloc[0])+'.csv')

from threading import Thread


class Processus(Thread):

    def __init__(self, bloc = (0,100)):
        Thread.__init__(self)
        self.bloc = bloc

    def run(self):
        get_rank(self.bloc)


processus1 = Processus((900001,1050000))

processus2 = Processus((1050001,1221773))

processus1.start()
processus2.start()



