# -*- coding: utf-8 -*-
"""
Created on Tue Apr  2 15:10:04 2019

@author: Lenovo
"""

'''
Ici le code tourne sur les fichier csv fournis par Joris.

Input : fichier csv du type export_commande_nuitducode_etat.csv, il contient les données 'asin', 'date_vendu',
'date_insertion', 'etat', prix_ttc', 'fdp_ttc' de la journée qui vient de s'écouler par exemple
        fichier csv du type log_refus_livre.csv, il contient les données concernant les livres qui ont
étérefusées et donc jamais mis en ligne. On y trouve la date du refus et l'isbn13 du livre.

Output: fichier csv sur lequel on va pouvoir entrainer le modèle pour prédire les volumes entrants de livres. Ce fichier est 
issu de la concaténation des nouvelles données récoltées dans la journée et des anciennes déjà mise en forme précedemment.
En Output le fichier contiendra les informations suivantes:
    asin, date_vendu, date_insertion, etat, mois (pour le mois de reception), annee (idem), occur(nombre d'apparition d'un asin
    pour un mois et une année donnée parmi les livres acceptés), occur_refus (même chose mais pour les livres refusés), 
    nb_tot_accept ( nb total de livres acceptés ie mis en ligne pour un asin sur l'ensemble des données dispo), nb_tot_refus(
    même chose pour les livres refusés), occur_tot (occur + occur_refus), nb_tot_books (nb_tot_accept+ nb_tot_refus), 
    occur_tot_normalise (occur_tot/nb_tot_books), Titre, AnneeParution, Langue, gap (le temps écoulé entre la parution
    du livre et sa réception par Recyclivre).
    
Conseil : Certaines sections sont des traitements assez longs a effectuer sur tout le tableau j'ai indiqué des ordres de grandeur
du temps nécessaire pour obtenir les sorties. Il ne faut pas refaire tourner certaines sections sur toutes les données à chaque 
fois qu'il y aura de nouvelles données.
'''
#%%Import
import time
import pandas as pd
import os
import isbnlib
#os.chdir("/home/techfg/1819_recyclivre/code/Utils")
from get_isbn_info import convert_isbn_to_infos_df
from amazon_rank import get_rank
#from utils.convert_isbn_infos import convert_isbn_to_infos

#%%traitement de export_commande_nuitducode_etat.csv --> treated_data.csv ~ 30 min (pour 3 million de lignes)
'''
Input : fichier csv historique des livres acceptés du type export_commande_nuitducode_etat.csv
Output : dataFrame qui associe a chaque ligne du csv précédent les volumes entrants (volume entrant dans le mois courant et 
volume total de cet asin présent dans tout l'historique)
'''
start_time = time.time()

#importation des données
data_recyclivre = pd.read_csv("/home/techfg/1819_recyclivre/code/data/export_commande_nuitducode_etat.csv")
data_recyclivre = data_recyclivre.loc[0:10000]

##supression des données abérantes

def nettoyage(raw_df):
    
    #Choix des colonns utiles pour le problème
    
    columns = ["asin", "date_vendu", "date_insertion", "etat"]
    treated_df = raw_df[columns].copy(deep=True)

    # Supression des dates qui valent 0
    
    treated_df = treated_df.loc[treated_df["date_insertion"] != "0000-00-00"]
    treated_df = treated_df.loc[treated_df["date_vendu"] != "0000-00-00 00:00:00"]
    
    # Supression des valeurs manquantes
    
    treated_df = treated_df.dropna(how='any')
    
    return(treated_df)

treated_data = nettoyage(data_recyclivre)
treated_data["date_insertion"] = pd.to_datetime(treated_data.date_insertion)
treated_data["date_vendu"] = pd.to_datetime(treated_data.date_vendu)

##Decompte du nombre d'apparitions d'un asin pour un mois et une année donnée, tous états confondus
treated_data['mois_reception'] = pd.DatetimeIndex(treated_data['date_insertion']).month
treated_data['annee_reception'] = pd.DatetimeIndex(treated_data['date_insertion']).year
copy_data = treated_data.copy(deep=True)
counts_date=copy_data.groupby(['annee_reception','mois_reception','asin']).size()


def volume(information_line):
    """
    Input : une ligne de DataFrame
    Output : le volume entrant pour cet isbn pendant le mois correspondant
    """
    volume = counts_date.loc[information_line.annee_reception, information_line.mois_reception, information_line.asin]
    return (volume)
 
#on applique volume a toutes les lignes tableau
treated_data['occur']= treated_data.apply(lambda x: volume(x), axis=1)


## Ajout du total de vente pour chaque isbn et creation d'un csv pour les livres acceptés : treated_data.csv

df = treated_data['asin'].value_counts()
occurence_accept = df.rename_axis('asin').reset_index(name='nb_tot_accept')
data_join=treated_data.join(occurence_accept.set_index('asin'), on='asin')
col = ['asin', 'date_vendu', 'date_insertion', 'etat', 'mois_reception', 'annee_reception', 'occur', 'nb_tot_accept']
data_final = data_join[col]
#os.chdir('/home/techfg/1819_recyclivre/code/data/')
#data_final.to_csv('treated_data.csv')


#%% Mêmes traitements sur le fichier log_refus_livre.csv ~ 1h-2h

data_refus = pd.read_csv('/home/techfg/1819_recyclivre/code/data/log_refus_livre.csv')
data_refus = data_refus.loc[0:10000]
data_refus = data_refus.rename(columns = {'2014-12-10': 'date', '9782862609119': 'isbn13'})

##nettoyage des dates fausses
data_refus_cleaned = data_refus.loc[data_refus['date'] != "0000-00-00"]
data_refus_cleaned['date']=pd.to_datetime(data_refus_cleaned.date)

##nettoyage des isbn faux
data_refus_cleaned['isbn13'] = data_refus_cleaned['isbn13'].astype(str)

def isbn13_to_isbn10 (information_line):
    if isbnlib.is_isbn13(information_line.isbn13):
        isbn10 = isbnlib.to_isbn10(information_line.isbn13)

        if isbnlib.is_isbn10(str(isbn10)):
            return (isbn10)
        else : 
            return(False)
    else :
        return (False)
    
data_refus_cleaned['asin'] = data_refus_cleaned.apply(lambda x: isbn13_to_isbn10(x), axis=1)
data_refus_cleaned = data_refus_cleaned.loc[data_refus_cleaned['asin']!=False]

##Decompte du nombre d'apparitions d'un asin pour un mois et une année et décompte total sur l'ensemble des données
data_refus_cleaned['mois_refus'] = pd.DatetimeIndex(data_refus_cleaned['date']).month
data_refus_cleaned['annee_refus'] = pd.DatetimeIndex(data_refus_cleaned['date']).year

#nombre total d'apparitions de chaque asin pour les livres refusés
df = data_refus_cleaned['asin'].value_counts()
occurence_refus = df.rename_axis('asin').reset_index(name='nb_tot_refus')
refus_vol_tot=data_refus_cleaned.join(occurence_refus.set_index('asin'), on='asin')

#occurence pour chaque mois
copy_data_refus = refus_vol_tot.copy(deep=True)
counts_date_refus=copy_data_refus.groupby(['annee_refus','mois_refus','asin']).size()

def volume_refus(information_line):
    """
    Input : une ligne de DataFrame
    Output : le volume entrant pour cet isbn pendant le mois correspondant
    """
    volume = counts_date_refus.loc[information_line.annee_refus, information_line.mois_refus, information_line.asin]
    return (volume)

treated_data_refus = refus_vol_tot.reset_index()
columns = ['date', 'isbn13', 'asin', 'mois_refus', 'annee_refus', 'nb_tot_refus']
treated_data_refus= treated_data_refus[columns]

treated_data_refus['occur_refus']= treated_data_refus.apply(lambda x: volume_refus(x), axis=1)
#os.chdir('/home/techfg/1819_recyclivre/code/data/')
#treated_data_refus.to_csv('treated_data_refus.csv')

#%%Join livres acceptés et refusés ~ rapide

#accepted_books = pd.read_csv("/home/techfg/1819_recyclivre/code/data/treated_data.csv")
#refused_books = pd.read_csv("/home/techfg/1819_recyclivre/code/data/treated_data_refus.csv")
accepted_books = data_final
refused_books = treated_data_refus
accepted_books['date_insertion']= pd.to_datetime(accepted_books.date_insertion)


columns = ['asin', 'mois_refus', 'annee_refus', 'nb_tot_refus', 'occur_refus']
refused_books_bis = refused_books[columns]
refused_books_bis = refused_books_bis.rename(columns = {'mois_refus': 'mois', 'annee_refus': 'annee'} )

accepted_books = accepted_books.rename(columns = {'mois_reception': 'mois', 'annee_reception' : 'annee'})

#on fait un outer join des 2 tableaux sur asin , mois et annee, pour les lignes qui sont présente
#dans un tableau et pas dans l'autre il y aura un NaN.    
merged_books = pd.merge(accepted_books, refused_books_bis, on=['asin', 'annee', 'mois'], how = 'outer')
merged_books = merged_books.drop_duplicates()
col = ['asin', 'date_vendu', 'date_insertion', 'etat', 'mois', 'annee', 'occur','occur_refus', 'nb_tot_accept', 'nb_tot_refus', ]
merged_books = merged_books[col]

#je remplace les NaN par des 0 pour pouvoir faire les totaux sur les occurences
merged_books = merged_books.fillna(0)
merged_books['occur_tot'] = merged_books['occur']+ merged_books['occur_refus']
merged_books['nb_tot_books'] = merged_books['nb_tot_accept']+merged_books['nb_tot_refus']
#la colonne occur_tot_normalise permet d'avoir des échelles cohérentes selon le nombre total d'exemplaires reçu sur les dernières années.
merged_books['occur_tot_normalise'] = merged_books['occur_tot']/merged_books['nb_tot_books']


#%% Creation d'un fichier csv global sur les volumes entrants --> books_entrant_info.csv

'''
Ici je met ensemble les volumes de ventes qu'on a pu récupérer jusqu'a maintenant et les données du fichier bddisbn.csv.

Du coup, je créé un csv qui va contenir, les différentes dates (vente, insertion, mois, année), les volumes totaux
(par mois/an/asin, par asin), ainsi que les données de l'asin (en particulier date d'édition et temps depuis parution)
par la suite j'aimerai ajouter le ranking amazon qui correspond à chaque date de vente.


On va considérer que l'on dispose déjà d'un csv avec un certain nombre d'infos liées a chaque asin, l'idée et de le compléter
pour les derniers livres reçus
'''


##Récupération des données liées à l'isbn dans un tableau a part 

'''
Je considère qu'il existe déjà un fichier bddisbn avec un certain nombre d'informations (titre, année de parution, langue)
stockées, on va le compléter puis l'écraser.
'''

isbn_info= pd.read_csv('/home/techfg/1819_recyclivre/code/data/bddisbn.csv')
colo= ['asin', 'Titre', 'AnneeParution', 'Langue']
isbn_info = isbn_info[colo]

asin=merged_books['asin'].unique()
for isbn in asin : 
    if isbn in isbn_info['asin']:
        asin.remove(isbn)

asin = pd.DataFrame(asin)
asin = asin.rename(columns={0 : 'asin'})
bdd = convert_isbn_to_infos_df(asin)
bdd = bdd[colo]
bddisbn = pd.concat([isbn_info, bdd])
print (bddisbn)
os.chdir('/home/techfg/1819_recyclivre/code/data/')
bddisbn.to_csv('bddisbn.csv')

#bddisbn = isbn_info

#Ici on ajoutes les données de l'isbn aux tableaux contenants les volumes
books_entrants_info = merged_books.join(bddisbn.set_index('asin'), on= 'asin')
books_entrants_info = books_entrants_info.drop_duplicates()
books_entrants_info['gap'] = books_entrants_info['annee'] - books_entrants_info['AnneeParution']


print(books_entrants_info.head())
'''
#Récupération des rangs, cette partie peut etre enlevée pour gagner du temps
books_info = get_rank(books_entrants_info)
'''
books_info = books_entrants_info
books_info = books_info.reset_index(drop = True)
os.chdir('/home/techfg/1819_recyclivre/code/data/')
books_info.to_csv('books_entrants_infos_test.csv')

print ("My program took", time.time() - start_time, "to run")

#%% mise a jours des occurences et création d'un fichier à jours avec tout dedans, dans l'optique ou on met à jours les données
#d'entrainement très régulièrement.
'''
Cette section prend beaucoup de temps à exeuter, c'est qqch qu'il faut améliorer par la suite. A l'heure actuelle il parait plus avantageux 
de faire tourner les sections précédentes sur toutes les nouvelles données d'entrainement (même si en théorie une partie a déjà été traitée).
'''

'''
books_all = pd.read_csv('/home/techfg/1819_recyclivre/code/data/books_entrants_infos.csv')

#occurence tot
occurence_join = occurence_accept.join(occurence_refus.set_index('asin'), on = 'asin')
occurence_join = occurence_join.fillna(0)
#je parcours les asin présents dans books_info et je met à jours les totaux dans le tableau avec toutes les infos et 
for k in range (len(occurence_join)):
    ligne = occurence_join.loc[k]
    isbn = ligne.asin
    accept = ligne.nb_tot_accept
    refus = ligne.nb_tot_refus
    books_all.loc[books_all['asin'] == isbn, 'nb_tot_accept'] += accept
    books_all.loc[books_all['asin'] == isbn, 'nb_tot_refus'] += refus
    books_all.loc[books_all['asin'] == isbn, 'nb_tot_books'] += (accept + refus)

counts_date = counts_date.rename(columns = {'mois_reception': 'mois', 'annee_reception' : 'annee'})
counts_date_refus= counts_date_refus.rename(columns = {'mois_refus': 'mois', 'annee_refus' : 'annee'})
for indice in counts_date.index:
    year = indice[0]
    month = indice[1]
    isbn = indice[2]
    number = counts_date[indice]
    books_all.loc[(books_all['asin'] == isbn) & (books_all['annee']==year) & (books_all['mois']==month), 'occur'] += number

for indice in counts_date_refus.index:
    year = indice[0]
    month = indice[1]
    isbn = indice[2]
    number = counts_date_refus[indice]
    books_all.loc[(books_all['asin'] == isbn) & (books_all['annee']==year) & (books_all['mois']==month), 'occur_refus'] += number

books_all['occur_tot'] = books_all['occur']+books_all['occur_refus']
books_all['occur_tot_normalise'] = books_all['occur_tot']/books_all['nb_tot_books']


for k in range(len(books_info)) :
    ligne = books_info.loc[k]
    isbn = str(ligne.asin)
    annee = ligne.annee
    mois = ligne.mois
    book = books_all.loc[(books_all['asin'] == isbn) & (books_all['annee']==year) & (books_all['mois']==month)]
    book = book.reset_index(drop= True)
    if len(book)!= 0:
        books_info.loc[k, 'occur'] = book['occur'].loc[0]
        books_info.loc[k, 'occur_refus'] = book['occur_refus'].loc[0]
        books_info.loc[k, 'nb_tot_accept'] = book['nb_tot_accept'].loc[0]
        books_info.loc[k, 'nb_tot_refus'] = book['nb_tot_refus'].loc[0]
    else: 
        
        book_bis = books_all.loc[books_all['asin']==isbn]
        if len(book)  != 0:     
            books_info.loc[k, 'nb_tot_accept'] = book_bis['nb_tot_accept'].loc[0]
            books_info.loc[k, 'nb_tot_refus'] = book_bis['nb_tot_refus'].loc[0]

books_info['nb_tot_books']= books_info['nb_tot_accept'] + books_info['nb_tot_refus']
books_info['occur_tot'] = books_info['occur']+books_info['occur_refus']
books_info['occur_tot_normalise'] = books_info['occur_tot']/books_info['nb_tot_books']

print (books_all.head())
print (books_info.head())


#Pour enregistrer le jeu de données complété

books=pd.concat([books_all, books_info])
os.chdir('/home/techfg/1819_recyclivre/code/data/')
books.to_csv('books_entrants_infos.csv')

'''

